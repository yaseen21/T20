 USE T20

SELECT
    batsman,
    SUM(CASE WHEN type != 'wide' THEN runs ELSE 0 END) AS Runs,
    COUNT(ball) AS Balls,
    COUNT(CASE WHEN type = 'wide' THEN 1 END) AS Wide,
    COUNT(CASE WHEN runs = '6' THEN 1 END) AS Six,
    COUNT(CASE WHEN runs = '4' THEN 1 END) AS Four,
    ROUND(CONVERT(FLOAT, SUM(CASE WHEN type != 'wide' THEN runs ELSE 0 END)) / CONVERT(FLOAT, COUNT(ball)), 4) * 100 AS SR
FROM match
WHERE innings = '1'
GROUP BY batsman;
SELECT
    batsman,
    SUM(CASE WHEN type != 'wide' THEN runs ELSE 0 END) AS Runs,
    COUNT(ball) AS Balls,
    COUNT(CASE WHEN type = 'wide' THEN 1 END) AS Wide,
    COUNT(CASE WHEN runs = '6' THEN 1 END) AS Six,
    COUNT(CASE WHEN runs = '4' THEN 1 END) AS Four,
    ROUND(CONVERT(FLOAT, SUM(CASE WHEN type != 'wide' THEN runs ELSE 0 END)) / CONVERT(FLOAT, COUNT(ball)), 4) * 100 AS SR,
    COUNT(CASE WHEN type != 'wide' AND runs = 0 THEN 1 END) AS MaidenOvers
FROM match
WHERE innings = '1'
GROUP BY batsman;